/**
 * Created by johan on 30/11/13.
 */

/**
 * Stolen from http://stackoverflow.com/questions/1184624/convert-form-data-to-js-object-with-jquery
 * Function to serialize a form into a javascript object.
 */
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

/**
 * Load JSON from an URL.
 * @param url
 * @param callback
 */
function loadJson(url, callback)
{
    $.ajax({
        url: url,
        type: "GET",
        success: callback
    });
}

/**
 * Load account information via JSON.
 * @param accountNumber
 * @param callback
 */
function loadAccount(accountNumber, callback)
{
    loadJson('/rest.php/account/' + accountNumber, callback);
}

/**
 * REST request function.
 * @param method HTTP method.
 * @param callback Callback after the request succeeded.
 * @param restData Optional request data.
 * @param subUrl Optional sub part of the REST url
 */
function restRequest(method, callback, restData, subUrl)
{
    if (typeof subUrl == 'undefined')
        subUrl = 'account';

    var request =
    {
        url: '/rest.php/' + subUrl,
        contentType: 'application/json',
        type: method,
        success: callback
    };

    if (restData !== null)
    {
        request.data = restData;
        request.processData = false;
    }
    $.ajax(request);

}

/**
 * Create a new account.
 * @param account
 * @param callback
 */
function doCreateAccount(account, callback)
{
    restRequest('POST', callback, account);
}

/**
 * Update an account.
 * @param account
 * @param callback
 */
function doUpdateAccount(account, callback)
{
    restRequest('PUT', callback, account);
}

/**
 * Transfer money from one account to the other.
 * @param transfer
 * @param callback
 */
function doTransferMoney(transfer, callback)
{
    restRequest('POST', callback, transfer, 'transfer');
}

/**
 * Get transactions of an account.
 * @param account
 * @param date
 * @param callback
 */
function doGetTransactions(account, date, callback)
{
    loadJson('/rest.php/transactions/' + account + '/' + date, callback);
}