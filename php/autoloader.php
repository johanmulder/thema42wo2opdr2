<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 30/11/13
 * Time: 20:12
 */

// Simple autoloader
spl_autoload_register(function ($class)
{
	require_once(__DIR__ . '/lib/' . $class . '.php');
});