<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 30/11/13
 * Time: 20:42
 */

require_once __DIR__  . '/autoloader.php';

$r = new Rest(new AccountService());
$r->handleRequest();