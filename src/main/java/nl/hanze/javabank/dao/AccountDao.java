package nl.hanze.javabank.dao;

import nl.hanze.javabank.Account;

/**
 * AccountDao interface.
 * 
 * @author Johan Mulder
 */
public interface AccountDao
{
	/**
	 * Create a new account.
	 * @param account
	 */
	public void create(Account account);
	
	/**
	 * Update an existing account.
	 * @param account
	 */
	public void update(Account account);
	
	/**
	 * Get an account by account number
	 * @param accountNumber
	 * @return
	 */
	public Account getByNumber(String accountNumber);
}
