package nl.hanze.javabank.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import nl.hanze.javabank.Account;

public class AccountDaoImpl implements AccountDao
{
	private final Connection connection;
	
	public AccountDaoImpl(Connection connection)
	{
		this.connection = connection;
	}

	@Override
	public void create(Account account)
	{
		try
		{
			String query = "INSERT INTO account (name, address, city, balance, `limit`, account_number) "
					+ "VALUES(?, ?, ?, ?, ?, ?)";
			PreparedStatement st = connection.prepareStatement(query);
			st.setString(1, account.getName());
			st.setString(2, account.getAddress());
			st.setString(3, account.getCity());
			st.setFloat(4, account.getBalance());
			st.setFloat(5, account.getLimit());
			st.setString(6, account.getAccountNumber());

			st.executeUpdate();
			st.close();
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}

		// EntityManager way of creating an account.
		// em.persist(account);
	}

	@Override
	public Account getByNumber(String accountNumber)
	{
		try
		{
			PreparedStatement st = connection
					.prepareStatement("SELECT * FROM account WHERE account_number = ?");
			st.setString(1, accountNumber);
			st.execute();
			ResultSet rs = st.getResultSet();
			if (rs == null || !rs.next())
			{
				st.close();
				return null;
			}

			// Convert the result set entry to an account object.
			// Yes, using the entitymanager would be great, but it can not be
			// used unfortunately.
			Account account = new Account();
			account.setAccountNumber(rs.getString("account_number"));
			account.setAddress(rs.getString("address"));
			account.setBalance(rs.getFloat("balance"));
			account.setCity(rs.getString("city"));
			account.setLimit(rs.getFloat("limit"));
			account.setName(rs.getString("name"));
			
			// Close the result set and the statement.
			rs.close();
			st.close();

			return account;
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}

		// EntityManager way of retrieving by account number.
		// return em.find(Account.class, accountNumber);
	}

	@Override
	public void update(Account account)
	{
		try
		{
			System.err.println(getClass().getName() + "(update) connection is " + connection.toString());
			
			String query = "UPDATE account SET name = ?, address = ?, city = ?, balance = ?, `limit` = ? WHERE "
					+ "account_number = ?";
			PreparedStatement st = connection.prepareStatement(query);
			st.setString(1, account.getName());
			st.setString(2, account.getAddress());
			st.setString(3, account.getCity());
			st.setFloat(4, account.getBalance());
			st.setFloat(5, account.getLimit());
			st.setString(6, account.getAccountNumber());

			st.executeUpdate();
			st.close();
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
		// EntityManager way of updating an account
		// em.persist(account);
	}

}
