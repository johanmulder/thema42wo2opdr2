package nl.hanze.javabank.dao;

import java.util.Date;

import nl.hanze.javabank.Transaction;

public interface TransactionDao
{
	/**
	 * Create a new transaction.
	 * @param tx
	 */
	public void create(Transaction tx);
	
	/**
	 * Get all transactions for a given account number.
	 * @param accountNumber
	 * @param after
	 * @return
	 */
	public Transaction[] getTransactions(String accountNumber, Date after);
}
