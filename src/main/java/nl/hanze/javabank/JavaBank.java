package nl.hanze.javabank;

import java.util.Date;

public interface JavaBank
{
	public void openAccount(Account account);

	public boolean transfer(float amount, String srcAccountNumber, String dstAccountNumber);

	public Account getAccount(String accountNumber);

	public void alterAccount(Account account);

	public Transaction[] getTransactions(String accountNumber, Date date);

}
