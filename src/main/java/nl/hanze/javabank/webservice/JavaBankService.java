package nl.hanze.javabank.webservice;

import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import nl.hanze.javabank.Account;
import nl.hanze.javabank.JavaBank;
import nl.hanze.javabank.JavaBankImpl;
import nl.hanze.javabank.Transaction;

@WebService(serviceName = "JavaBankService")
public class JavaBankService implements JavaBank
{
	// The JavaBank implementation.
	private JavaBank javaBank = new JavaBankImpl();

	@WebMethod
	@Override
	public void openAccount(@WebParam(name = "account") Account account)
	{
		javaBank.openAccount(account);
	}

	@WebMethod
	@Override
	public boolean transfer(@WebParam(name = "amount") float amount,
			@WebParam(name = "srcAccountNumber") String srcAccountNumber,
			@WebParam(name = "dstAccountNumber") String dstAccountNumber)
	{
		return javaBank.transfer(amount, srcAccountNumber, dstAccountNumber);
	}

	@WebMethod
	@Override
	public Account getAccount(@WebParam(name = "accountNumber") String accountNumber)
	{
		if (accountNumber == null)
			throw new RuntimeException("accountNumber is null");
		return javaBank.getAccount(accountNumber);
	}

	@WebMethod
	@Override
	public void alterAccount(@WebParam(name = "account") Account account)
	{
		if (account == null)
			throw new RuntimeException("Account is null");
		if (account.getAccountNumber() == null)
			throw new RuntimeException("Account number is null");
		javaBank.alterAccount(account);
	}

	@WebMethod
	@Override
	public Transaction[] getTransactions(@WebParam(name = "accountNumber") String accountNumber,
			@WebParam(name = "date") Date date)
	{
		if (date == null)
			throw new RuntimeException("Date is null");
		return javaBank.getTransactions(accountNumber, date);
	}
}
