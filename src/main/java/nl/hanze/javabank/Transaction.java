package nl.hanze.javabank;

import java.util.Date;

public class Transaction
{
	private long id;
	private Date date;
	private String srcAccount;
	private String dstAccount;
	private float amount;

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public float getAmount()
	{
		return amount;
	}

	public void setAmount(float amount)
	{
		this.amount = amount;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getSrcAccount()
	{
		return srcAccount;
	}

	public void setSrcAccount(String srcAccount)
	{
		this.srcAccount = srcAccount;
	}

	public String getDstAccount()
	{
		return dstAccount;
	}

	public void setDstAccount(String dstAccount)
	{
		this.dstAccount = dstAccount;
	}
}
