package nl.hanze.javabank;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import nl.hanze.javabank.dao.AccountDao;
import nl.hanze.javabank.dao.AccountDaoImpl;
import nl.hanze.javabank.dao.TransactionDao;
import nl.hanze.javabank.dao.TransactionDaoImpl;

/**
 * Implementation of JavaBank.
 * 
 * @author Johan Mulder
 * 
 */
public class JavaBankImpl implements JavaBank
{
	private final Connection connection;
	// The accountDao
	private AccountDao accountDao;
	private TransactionDao transactionDao;

	/**
	 * No-arg constructor.
	 */
	public JavaBankImpl()
	{
		try
		{
			// Look up the JavaBank JDBC service via JNDI.
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("JavaBank");
			connection = ds.getConnection();
			init();
		}
		catch (SQLException | NamingException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Constructor with Connection object to improve testability of this class.
	 * 
	 * @param connection
	 */
	public JavaBankImpl(Connection connection)
	{
		this.connection = connection;
		try
		{
			init();
		}
		catch (SQLException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Initialize auto commit and the DAO objects.
	 * @throws SQLException
	 */
	private void init() throws SQLException
	{
		connection.setAutoCommit(false);
		// This should be injectable as well.
		accountDao = new AccountDaoImpl(connection);
		transactionDao = new TransactionDaoImpl(connection);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.hanze.javabank.JavaBank#openAccount(nl.hanze.javabank.Account)
	 */
	@Override
	public void openAccount(Account account)
	{
		// Check if the accounts exists already.
		Account existing = accountDao.getByNumber(account.getAccountNumber());
		if (existing != null)
			throw new RuntimeException("Account " + account.getAccountNumber() + " exists already");
		accountDao.create(account);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.hanze.javabank.JavaBank#transfer(float, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean transfer(float amount, String srcAccountNumber, String dstAccountNumber)
	{
		try
		{
			// Get source and destination account.
			Account src = accountDao.getByNumber(srcAccountNumber);
			if (src == null)
				throw new Exception("Source account " + srcAccountNumber + " not found");
			Account dst = accountDao.getByNumber(dstAccountNumber);
			if (dst == null)
				throw new Exception("Destination account " + dstAccountNumber + " not found");

			// Don't transfer if the current balance is lower than the given
			// amount, or if the amount exceeds the limit of the account.
			if (src.getBalance() < amount || amount > src.getLimit())
			{
				System.err.println("The amount to transfer (" + amount
						+ ") exceeds the limit of account " + src.getAccountNumber() + "("
						+ src.getLimit() + ")");
				return false;
			}
			// Subtract the amount at the source account.
			src.setBalance(src.getBalance() - amount);
			// And add it to the destination account
			dst.setBalance(dst.getBalance() + amount);

			// Start transaction.
			connection.setAutoCommit(false);

			// Update the accounts.
			accountDao.update(src);
			// Uncomment to demonstrate atomicity of the transaction.
			// if (true) throw new RuntimeException("Test");
			accountDao.update(dst);
			// Create the transaction.
			createTransaction(amount, src, dst);

			// Commit the changes.
			connection.commit();
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}

		return true;
	}

	/**
	 * @param amount
	 * @param src
	 * @param dst
	 * @return
	 */
	private void createTransaction(float amount, Account src, Account dst)
	{
		// Create a new transaction.
		Transaction tx = new Transaction();
		tx.setAmount(amount);
		tx.setDate(new Date());
		tx.setSrcAccount(src.getAccountNumber());
		tx.setDstAccount(dst.getAccountNumber());
		// Create the transaction.
		transactionDao.create(tx);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.hanze.javabank.JavaBank#getAccount(java.lang.String)
	 */
	@Override
	public Account getAccount(String accountNumber)
	{
		return accountDao.getByNumber(accountNumber);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.hanze.javabank.JavaBank#alterAccount(nl.hanze.javabank.Account)
	 */
	@Override
	public void alterAccount(Account account)
	{
		accountDao.update(account);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.hanze.javabank.JavaBank#getTransactions(java.lang.String,
	 * java.sql.Date)
	 */
	@Override
	public Transaction[] getTransactions(String accountNumber, Date date)
	{
		// Check if the account exists.
		if (accountDao.getByNumber(accountNumber) == null)
			throw new RuntimeException("Account " + accountNumber + " does not exist");
		// Get all transactions for the given date.
		return transactionDao.getTransactions(accountNumber, date);
	}

}
